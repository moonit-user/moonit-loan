package nl.moonit.step;

import nl.moonit.page.LoanPage;
import nl.moonit.state.StepdefinitionState;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class LoanStepdefinitions {

    private String urlLoanPage;
    private LoanPage loanPage;
    private StepdefinitionState stepdefinitionState;
    private WebDriver webDriver;

    @Autowired
    public LoanStepdefinitions(LoanPage loanPage,
                               StepdefinitionState stepdefinitionState,
                               @Value("${urlLoanPage}") String urlLoanPage, WebDriver webDriver) {
        this.loanPage = loanPage;
        this.stepdefinitionState = stepdefinitionState;
        this.urlLoanPage = urlLoanPage;
        this.webDriver = webDriver;
    }

}

package nl.moonit.config;

import nl.moonit.service.WebDriverService;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@PropertySource("application.properties")
@ComponentScan("nl.moonit")
public class Config {

    @Autowired
    private WebDriverService webDriverService;

    @Bean
    public WebDriver webDriver() {
        return webDriverService.getDriver();
    }

}

package nl.moonit.state;

import lombok.Data;
import nl.moonit.browser.Loan;
import org.springframework.stereotype.Service;

@Service
@Data
public class StepdefinitionState {

    private Loan loan;

}

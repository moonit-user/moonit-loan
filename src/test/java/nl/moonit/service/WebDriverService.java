package nl.moonit.service;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import nl.moonit.browser.Browser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

@Component
@Slf4j
public class WebDriverService {

    private boolean remoteDriverEnabled;
    private String browserOption;
    private String seleniumFirefoxHub;
    private String seleniumChromeHub;

    public WebDriverService(@Value("${remoteDriverEnabled}") boolean remoteDriverEnabled,
                            @Value("${browserOption}") String browserOption,
                            @Value("${seleniumFirefoxHub}") String seleniumFirefoxHub,
                            @Value("${seleniumChromeHub}") String seleniumChromeHub) {
        this.remoteDriverEnabled = remoteDriverEnabled;
        this.browserOption = browserOption;
        this.seleniumFirefoxHub = seleniumFirefoxHub;
        this.seleniumChromeHub = seleniumChromeHub;
    }

    public WebDriver getDriver() {
        WebDriver webDriver = null;
        if (remoteDriverEnabled) {
            try {
                webDriver = activateRemoteDriver(getBrowserOption());
            } catch (MalformedURLException e) {
                log.error("Invalid firefox or chrome url provided");
            }
        } else {
            webDriver = activateLocalDriver(getBrowserOption());
        }
        return webDriver;
    }

    public Browser getBrowserOption() {
        Browser browser;
        switch (browserOption) {
            case "firefox":
                browser = Browser.FIREFOX;
                break;
            case "chrome":
                browser = Browser.CHROME;
                break;
            default:
                throw new IllegalArgumentException("Not supported browseroption: " + browserOption);
        }
        return browser;
    }

    private WebDriver activateLocalDriver(Browser browser) {
        WebDriver webDriver;
        switch (browser) {
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                webDriver = new FirefoxDriver();
                break;
            case CHROME:
                WebDriverManager.chromedriver().setup();
                webDriver = new ChromeDriver();
                break;
            default:
                throw new IllegalArgumentException("Not supported browser: " + browser);
        }
        return webDriver;
    }

    private WebDriver activateRemoteDriver(Browser browser) throws MalformedURLException {
        WebDriver webDriver;
        switch (browser) {
            case FIREFOX:

                FirefoxOptions firefoxOptions = new FirefoxOptions();
                webDriver = new RemoteWebDriver(new URL(seleniumFirefoxHub), firefoxOptions);
                break;
            case CHROME:
                ChromeOptions chromeOptions = new ChromeOptions();
                webDriver = new RemoteWebDriver(new URL(seleniumChromeHub), chromeOptions);
                break;
            default:
                throw new IllegalArgumentException("Not supported browser: " + browser);
        }
        return webDriver;
    }
}

package nl.moonit.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import javax.annotation.PreDestroy;

public class AbstractPage {

    private WebDriver webDriver;

    public AbstractPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void clickAction(String id) {
        WebElement element = webDriver.findElement(By.id(id));
        Actions builder = new Actions(webDriver);
        builder.moveToElement(element);
        builder.pause(1000);
        builder.click(element);
        builder.build().perform();
    }

    @PreDestroy()
    private void tearDown() {
        webDriver.quit();
    }

}

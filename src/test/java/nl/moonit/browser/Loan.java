package nl.moonit.browser;

import lombok.Getter;

public enum Loan {
    FLASH(Constants.FLASH_LOAN_SHORT_DESCRIPTION, Constants.FLASH_LOAN_DESCRIPTION);
    // TODO add all required enums

    @Getter
    private String shortDescription;

    @Getter
    private String description;

    Loan(String shortDescription, String description) {
        this.shortDescription = shortDescription;
        this.description = description;
    }

    public static Loan toEnum(String description) {
        Loan loan = null;
        // TODO implement method
        return loan;
    }

    private static class Constants {
        public static final String FLASH_LOAN_SHORT_DESCRIPTION = "Flash loan";
        public static final String FLASH_LOAN_DESCRIPTION = "Flash loans are also subject to the maximum credit compensation";

        // TODO: add all needed descriptions
    }
}

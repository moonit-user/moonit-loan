package nl.moonit.browser;

public enum Browser {
    FIREFOX("firefox"), CHROME("chrome");

    private String browserName;

    Browser(String name) {
        this.browserName = name;
    }

}

package nl.moonit.domain;

import lombok.Data;

@Data
public class Loan {
    private String loanType;
    private String amount;
}

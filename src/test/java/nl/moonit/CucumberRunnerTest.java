package nl.moonit;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"de.monochromata.cucumber.report.PrettyReports:target/cucumber"},
        glue = "nl.moonit.step",
        features = "src/test/resources/nl/moonit",
        tags = "")
public class CucumberRunnerTest {

}
